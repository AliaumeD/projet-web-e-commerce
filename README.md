#### **Projet WEB : Site d'E-commerce**
Contextualisation : Demande de création de site d'e-commerce pour une marque familliale de vêtement brodé main.
####
Ci dessous vous trouverez les différentes pages avec les fonctionnalités expliquées.

#### **Image des pages**

**Acceuil du site**

![Erreur](public/img/img1.PNG)

**Les menus**
####
En bas à gauche on retrouve les menus accessibles par les utilisateurs (La page d'acceuil la page de shopping et la page de connexion ou de déconnexion si déjà connecté) et à droite le menu des pages d'administration pour le super admin du site où il pourra gérer les produits, les utilisateurs et les factures du site.

![Erreur](public/img/img2.PNG)

**Slider des produits (shopping)**

![Erreur](public/img/img3.PNG)

**Page de connexion**

![Erreur](public/img/img4.PNG)

**Page d'inscription**

![Erreur](public/img/img5.PNG)
####
Crée un nouvelle utilisateur dans la table Users à chaque nouvelle inscription 

**Administration : liste des produits**

![Erreur](public/img/img6.PNG)

**Administration : ajout/modification produit**
####
Crée un nouveau produit dans la table Products à chaque ajout de produit

![Erreur](public/img/img7.PNG)

**Administration : Utilisateurs**
####
Avec possibilité de suppression via la corbeille
![Erreur](public/img/img8.PNG)

**Administration : Facture**
####
Avec possibilité de suppression via la corbeille
![Erreur](public/img/img9.PNG)

**Commande**
####
Accessible via le slider produit, si utilisateur pas connecté, redirige vers la page de connexion sinon redirige vers la page ci dessous
![Erreur](public/img/img10.PNG)
####
Lorsque l'on clique sur commander ajoute une nouvelle facture dans la table Facture et envoie un mail à l'utilisateur connecté ainsi qu'à l'admin du site

####
Récap :
Outil d'administration permettant de gérer les produits (suppression/modification/ajout) et mise à jour en temps réels dans le slider de produits, possibilité de gérer les factures et les users également.
####
Une vidéo explicative sera fourni en complément du README.md


#### **Si les images ne s'affichent pas vous pouvez directement aller voir dans le fichier public/image/**