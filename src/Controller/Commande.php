<?php


namespace App\Controller;

use App\Entity\Products;
use App\Entity\Facture;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Commande extends AbstractController
{
    /**
     * @Route("/commande/{id}",name="commande")
     */
    public function commande($id)
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            return $this->redirect('/connexion');
        } else {

            $statut = "connecte";
        }
        $data = $_SESSION['user'];
        $repository = $this->getDoctrine()->getRepository(Products::class);
        $produit = $repository->findOneBy(['id' => $id]);
        if (isset ($_POST['commander'])) {
            date_default_timezone_set('Europe/Paris');
            $date = date('l jS \of F Y h:i:s A');
            /*ini_set("SMTP","ssl: imap.orange.fr");
            ini_set("smtp_port","993");
            mail($data->getEmail(),"commande","coucou Aliaume le boss","From: lespetitshautsdeju@yahoo.fr");*/
            $entityManager = $this->getDoctrine()->getManager();
            $facture = new Facture();
            $facture->setCommanditaire($data->getPseudo());
            $facture->setProduit($produit->getName());
            $facture->setPrice($produit->getPrice());
            $facture->setDate($date);
            $entityManager->persist($facture);
            $entityManager->flush();
        }
        return $this->render('commande.html.twig',
            [
                'name' => $produit->getName(),
                'price' => $produit->getPrice(),
                'description' => $produit->getDescription(),
                'image' => $produit->getImage(),
                'stock' => $produit->getStock(),
                'statut' => $statut
            ]);

    }
}