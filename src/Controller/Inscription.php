<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Users;

class Inscription extends AbstractController
{
    /**
     * @Route("/inscription",name="inscription")
     */
    public function account()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        $notif_erreur = "";
        $notif_reussite = "";
        if (isset($_POST['name'])) {
            if ($_POST['name'] != "") {
                $name = $_POST['name'];
            } else {
                $notif_erreur .= "Veuillez compléter le champs \"Nom\"<br>";
            }
        }

        if (isset($_POST['surname'])) {
            if ($_POST['surname'] != "") {
                $surname = $_POST['surname'];
            } else {
                $notif_erreur .= "Veuillez compléter le champs \"Prénom\" <br>";
            }
        }

        if (isset($_POST['pseudo'])) {
            if ($_POST['pseudo'] != NULL) {
                $pseudo = $_POST['pseudo'];
            } else {
                $notif_erreur .= "Veuillez compléter le champs \"Identifiant\"";
            }
        }

        if (isset($_POST['email'])) {
            if ($_POST['email'] != NULL) {
                $email = $_POST['email'];
            } else {
                $notif_erreur .= "Veuillez compléter le champs \"Email\"";
            }
        }

        if (isset($_POST['password'])) {
            if ($_POST['password'] != "") {
                $password = $_POST['password'];
            } else {
                $notif_erreur .= "Veuillez compléter le champs \"Mot de passe\"";
            }
        }

        if(isset($name) && isset($surname) && isset($pseudo) && isset($email) && isset($password))
        {
            $entityManager = $this->getDoctrine()->getManager();
            $user = new Users();
            $user->setName($name);
            $user->setSurname($surname);
            $user->setPseudo($pseudo);
            $user->setEmail($email);
            $user->setPassword(password_hash($password,PASSWORD_BCRYPT ));
            $user->setRole('user');
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirect('/');
        }

        if(isset($name) && isset($surname) && isset($pseudo) && isset($email) && isset($password) && $notif_erreur=="")
        {
            return $this->render('inscription.html.twig',
                [
                    'name' => $name,
                    'surname' => $surname,
                    'pseudo' => $pseudo,
                    'email' => $email,
                    'password' => $password,
                    'notif_erreur' =>$notif_erreur,
                    'statut' =>$statut,
                    'notif_reussite' =>$notif_reussite
                ]);
        }else{
            return $this->render('inscription.html.twig',
                [
                    'name' => '',
                    'surname' => '',
                    'pseudo' => '',
                    'email' => '',
                    'password' => '',
                    'notif_erreur' =>$notif_erreur,
                    'statut' =>$statut,
                    'notif_reussite' =>$notif_reussite
                ]);
        }
    }
}