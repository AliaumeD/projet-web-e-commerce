<?php


namespace App\Controller;

use App\Entity\Products;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ListeProduits extends AbstractController
{
    /**
     * @Route("/administration",name="administration")
     */
    public function liste_produits()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        $entityManager = $this->getDoctrine()->getManager();
        $produits = $entityManager->getRepository(Products::class)->findAll();
        $i = 0;
        foreach ($produits as $produit) {
            $arrProducts[$i]['id'] = $produit->getId();
            $arrProducts[$i]['name'] = $produit->getName();
            $arrProducts[$i]['description'] = $produit->getDescription();
            $arrProducts[$i]['price'] = $produit->getPrice();
            $arrProducts[$i]['stock'] = $produit->getStock();
            $arrProducts[$i]['image'] = $produit->getImage();
            $i = $i + 1;
        }
        if (isset($_POST['id'])) {
            $repository = $this->getDoctrine()->getRepository(Products::class);
            $id = $_POST['id'];
            $produit = $repository->findOneBy(['id' => $_POST['id']]);
            $entityManager->remove($produit);
            $entityManager->flush();
            return $this->redirect('/administration');
        }
        return $this->render('liste_produits.html.twig',
            [
                'arrProducts' => $arrProducts,
                'statut' => $statut,
                'id' => "false"
            ]);
    }

}