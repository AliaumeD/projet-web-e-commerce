<?php


namespace App\Controller;


use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Connexion extends AbstractController
{
    /**
     * @Route("/connexion",name="connexion")
     */
    public function connexion()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Users::class);
        $users = $entityManager->getRepository(Users::class)->findAll();
        $notif_erreur = "";
        if (isset($_POST['email']) && isset($_POST['password'])) {
            foreach ($users as $user) {
                if ($user->getEmail() == $_POST['email']) {
                    $email = $_POST['email'];
                }else{
                    $notif_erreur = "Email ou mot de passe incorrect";
                }
                if($user->getPassword() == $_POST['password'])
                {
                    $password = $_POST['password'];
                }else{
                    $notif_erreur = "Email ou mot de passe incorrect";
                }
            }
        }
        if (isset($email) && isset($password)) {
            $user = $repository->findOneBy([
                'email' => $email,
                'password' => $password
            ]);
            if ($user) {
                $_SESSION['user'] = $user;
                return $this->redirect('/');
            }else{
                $notif_erreur = "Email ou mot de passe incorrect";
            }
        }
        //var_dump($_SESSION);
        return $this->render('connexion.html.twig',
            [
                'notif_erreur' => $notif_erreur,
                'statut' => $statut
            ]);

    }
}