<?php


namespace App\Controller;
use App\Entity\Products;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Magasin extends AbstractController
{
    /**
     * @Route("/magasin",name="magasin")
     */
    public function magasin()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        $entityManager = $this->getDoctrine()->getManager();
        $produits = $entityManager->getRepository(Products::class)->findAll();
        $arrProducts = array();
        $i=0;
        foreach ($produits as $produit) {
            $arrProducts[$i]['id'] = $produit->getId();
            $arrProducts[$i]['name'] = $produit->getName();
            $arrProducts[$i]['description'] = $produit->getDescription();
            $arrProducts[$i]['price'] = $produit->getPrice();
            $arrProducts[$i]['stock'] = $produit->getStock();
            $arrProducts[$i]['image'] = $produit->getImage();
            $i=$i+1;
        }
        //var_dump($arrProducts);
        return $this->render('magasin.html.twig',
            [
                'arrProducts' => $arrProducts,
                'statut' => $statut
            ]);
    }
}