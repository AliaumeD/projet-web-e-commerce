<?php


namespace App\Controller;

use App\Entity\Products;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class GestionProduits extends AbstractController
{
    /**
     * @Route("/administration_edit",name="administration_edit")
     */
    public function gestion_produits()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        $notif_erreur = "";
        $notif_reussite = "";
        $repository = $this->getDoctrine()->getRepository(Products::class);
        $produit = $repository->findOneBy(['id' => $_POST['id']]);

        if (isset($_POST['name'])) {
            if ($_POST['name'] != "") {
                $name = $_POST['name'];
            }
            else {
                $name = $produit->getName();
                $notif_erreur .= "Le nom du produit ne peut pas être vide";
            }
        }else{
            $name = $produit->getName();
        }

        if (isset($_POST['description'])) {
            $description = $_POST['description'];
        } else {
            $description = $produit->getDescription();
        }

        if (isset($_POST['price'])) {
            if ($_POST['price'] != NULL) {
                $price = $_POST['price'];
            } else {
                $price = $produit->getPrice();
                $notif_erreur .= "Le prix du produit ne peut pas être nul";
            }
        }else{
            $price = $produit->getPrice();
        }

        if (isset($_POST['stock'])) {
            if ($_POST['stock'] != NULL) {
                $stock = $_POST['stock'];
            } else {
                $stock = $produit->getStock();
                $notif_erreur .= "Le stock du produit ne peut pas être nul";
            }
        }else{
            $stock = $produit->getStock();
        }

        if (isset($_POST['image'])) {
            if ($_POST['image'] != "") {
                $image = $_POST['image'];
            } else {
                $image = $produit->getImage();
                $notif_erreur .= "L'image du produit ne peut pas être vide";
            }
        }else{
            $image = $produit->getImage();
        }

        if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['price']) && isset($_POST['stock']) && isset($_POST['image']) && $notif_erreur == "") {
            $entityManager = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine()->getRepository(Products::class);
            $produit = $repository->findOneBy(['id' => $_POST['id']]);
            $produit->setName($name);
            $produit->setDescription($description);
            $produit->setPrice($price);
            $produit->setStock($stock);
            $produit->setImage($image);
            $entityManager->flush();
            $notif_reussite = "Produit modifié !";
        }


        return $this->render('gestion_produits.html.twig',
            [
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'stock' => $stock,
                'image' => $image,
                'id' => $_POST['id'],
                'notif_reussite' => $notif_reussite,
                'notif_erreur' => $notif_erreur,
                'statut' => $statut
            ]);

    }


}