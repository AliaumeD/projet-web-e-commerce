<?php


namespace App\Controller;


use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Deconnexion extends AbstractController
{
    /**
     * @Route("/deconnexion",name="deconnexion")
     */
    public function deconnexion()
    {
        session_start();
        session_destroy(); // on détruit la/les session(s), soit si vous utilisez une autre session, utilisez de préférence le unset()
        $statut="deconnecte";
        return $this->render('index.html.twig',
            [
                'statut' => $statut
            ]);
    }
}