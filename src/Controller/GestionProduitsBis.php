<?php


namespace App\Controller;

use App\Entity\Products;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class GestionProduitsBis extends AbstractController
{
    /**
     * @Route("/administration_edit_bis",name="administration_edit_bis")
     */
    public function gestion_produits_bis()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        $notif_erreur = "";
        $notif_reussite = "";
        if (isset($_POST['name'])) {
            if ($_POST['name'] != "") {
                $name = $_POST['name'];
            } else {
                $notif_erreur = "Le nom du produit ne peut pas être vide";
            }
        }

        if (isset($_POST['description'])) {
            $description = $_POST['description'];
        }

        if (isset($_POST['price'])) {
            if ($_POST['price'] != NULL) {
                $price = $_POST['price'];
            } else {
                $notif_erreur .= "Le prix du produit ne peut pas être nul";
            }
        }

        if (isset($_POST['stock'])) {
            if ($_POST['stock'] != NULL) {
                $stock = $_POST['stock'];
            } else {
                $notif_erreur .= "Le stock du produit ne peut pas être nul";
            }
        }

        if (isset($_POST['image'])) {
            if ($_POST['image'] != "") {
                $image = $_POST['image'];
            } else {
                $notif_erreur .= "L'image du produit ne peut pas être vide";
            }
        }

        if(isset($name) && isset($description) && isset($price) && isset($stock) && isset($image))
        {
            $entityManager = $this->getDoctrine()->getManager();
            $product = new Products();
            $product->setName($name);
            $product->setDescription($description);
            $product->setPrice($price);
            $product->setStock($stock);
            $product->setImage($image);
            $entityManager->persist($product);
            $entityManager->flush();
            $notif_reussite="Produit ajouté !";
        }

        if(isset($_POST['name']) && isset($_POST['description']) && isset($_POST['price']) && isset($_POST['stock']) && isset($_POST['image']) && $notif_erreur=="")
        {
            return $this->render('gestion_produits_bis.html.twig',
                [
                    'name' => $name,
                    'description' => $description,
                    'price' => $price,
                    'stock' => $stock,
                    'image' => $image,
                    'statut' => $statut,
                    'notif_erreur' =>$notif_erreur,
                    'notif_reussite' =>$notif_reussite
                ]);
        }else{
            return $this->render('gestion_produits_bis.html.twig',
                [
                    'name' => '',
                    'description' => '',
                    'price' => 0,
                    'stock' => 0,
                    'image' => '',
                    'notif_erreur' =>$notif_erreur,
                    'notif_reussite' =>$notif_reussite,
                    'statut' =>$statut
                ]);
        }


    }


}