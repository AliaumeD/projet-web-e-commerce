<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Index extends AbstractController
{
    /**
     * @Route("/",name="index")
     */
    public function index()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        if (isset($_SESSION['user'])) {
            $data = $_SESSION['user'];
            //echo "<pre>";print_r($data);"</pre>";
            $pseudo = $data->getPseudo();
            $email = $data->getEmail();
            $name = $data->getName();
            return $this->render('index.html.twig',
                [
                    'pseudo' => $pseudo,
                    'email' => $email,
                    'name' => $name,
                    'statut' => $statut
                ]);
        } else {
            return $this->render('index.html.twig',
                [
                    'statut' => $statut
                ]);
        }
    }

}