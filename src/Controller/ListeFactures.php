<?php


namespace App\Controller;

use App\Entity\Facture;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ListeFactures extends AbstractController
{
    /**
     * @Route("/administration/gestion_factures",name="administration_facture")
     */
    public function liste_factures()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        $entityManager = $this->getDoctrine()->getManager();
        $factures = $entityManager->getRepository(Facture::class)->findAll();
        $i = 0;
        foreach ($factures as $facture) {
            $arrFactures[$i]['id'] = $facture->getId();
            $arrFactures[$i]['commanditaire'] = $facture->getCommanditaire();
            $arrFactures[$i]['produit'] = $facture->getProduit();
            $arrFactures[$i]['price'] = $facture->getPrice();
            $arrFactures[$i]['date'] = $facture->getDate();
            $i = $i + 1;
        }
        if (isset($_POST['id'])) {
            $repository = $this->getDoctrine()->getRepository(Facture::class);
            $id = $_POST['id'];
            $facture= $repository->findOneBy(['id' => $_POST['id']]);
            $entityManager->remove($facture);
            $entityManager->flush();
            return $this->redirect('/administration/gestion_factures');
        }
        return $this->render('liste_factures.html.twig',
            [
                'arrFactures' => $arrFactures,
                'statut' => $statut,
                'id' => "false"
            ]);
    }
}