<?php


namespace App\Controller;

use App\Entity\Users;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListeUtilisateurs extends AbstractController
{
    /**
     * @Route("/administration/gestion_utilisateurs",name="administration_user")
     */
    public function liste_produits()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['user'])) {
            $statut = "deconnecte";
        } else {
            $statut = "connecte";
        }
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository(Users::class)->findAll();
        $i = 0;
        foreach ($users as $user) {
            $arrUsers[$i]['id'] = $user->getId();
            $arrUsers[$i]['pseudo'] = $user->getPseudo();
            $arrUsers[$i]['password'] = $user->getPassword();
            $arrUsers[$i]['name'] = $user->getName();
            $arrUsers[$i]['surname'] = $user->getSurname();
            $arrUsers[$i]['email'] = $user->getEmail();
            $arrUsers[$i]['role'] = $user->getRole();
            $i = $i + 1;
        }
        if (isset($_POST['id'])) {
            $repository = $this->getDoctrine()->getRepository(Users::class);
            $id = $_POST['id'];
            $user= $repository->findOneBy(['id' => $_POST['id']]);
            $entityManager->remove($user);
            $entityManager->flush();
            return $this->redirect('/administration/gestion_utilisateurs');
        }
        return $this->render('liste_utilisateurs.html.twig',
            [
                'arrUsers' => $arrUsers,
                'statut' => $statut,
                'id' => "false"
            ]);
    }
}