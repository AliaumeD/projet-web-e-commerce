<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210622213046 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__facture AS SELECT id, commanditaire, produit, price, date FROM facture');
        $this->addSql('DROP TABLE facture');
        $this->addSql('CREATE TABLE facture (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, commanditaire VARCHAR(30) DEFAULT NULL COLLATE BINARY, produit VARCHAR(30) DEFAULT NULL COLLATE BINARY, price INTEGER DEFAULT NULL, date VARCHAR(255) DEFAULT NULL)');
        $this->addSql('INSERT INTO facture (id, commanditaire, produit, price, date) SELECT id, commanditaire, produit, price, date FROM __temp__facture');
        $this->addSql('DROP TABLE __temp__facture');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__facture AS SELECT id, commanditaire, produit, price, date FROM facture');
        $this->addSql('DROP TABLE facture');
        $this->addSql('CREATE TABLE facture (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, commanditaire VARCHAR(30) DEFAULT NULL, produit VARCHAR(30) DEFAULT NULL, price INTEGER DEFAULT NULL, date DATE DEFAULT NULL)');
        $this->addSql('INSERT INTO facture (id, commanditaire, produit, price, date) SELECT id, commanditaire, produit, price, date FROM __temp__facture');
        $this->addSql('DROP TABLE __temp__facture');
    }
}
